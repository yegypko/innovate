<?php
/**
	 * Registers a new post type
	 * @uses $wp_post_types Inserts new post type object into the list
	 *
	 * @param string  Post type key, must not exceed 20 characters
	 * @param array|string  See optional args description above.
	 * @return object|WP_Error the registered post type object, or an error object
	 */

	
	function innovate_theme_post_types() {

		// Post Promo
		$labels = array(
			'name'               => __( 'Promo', 'promo' ),
			'singular_name'      => __( 'Promo', 'promo' ),
			'add_new'            => _x( 'Add New Promo', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Promo', 'promo' ),
			'edit_item'          => __( 'Edit Promo', 'promo' ),
			'new_item'           => __( 'New Promo', 'promo' ),
			'view_item'          => __( 'View Promo', 'promo' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Promo', 'promo' ),
			'all_items'			=> 'All Promo',
		);
	
		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-welcome-view-site',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'show_in_rest'		=> false,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				// 'editor',
				// 'author',
				// 'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'promo', $args );


			// About Us
			$labels = array(
				'name'               => __( 'About', 'about' ),
				'singular_name'      => __( 'About', 'about' ),
				'add_new'            => _x( 'Add New About', 'text-domain', 'text-domain' ),
				'add_new_item'       => __( 'Add New About', 'about' ),
				'edit_item'          => __( 'Edit About', 'about' ),
				'new_item'           => __( 'New About', 'about' ),
				'view_item'          => __( 'View About', 'about' ),
				'search_items'       => __( 'Search Plural Name', 'text-domain' ),
				'not_found'          => __( 'No Plural Name found', 'text-domain' ),
				'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
				'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
				'menu_name'          => __( 'About', 'about' ),
				'all_items'			=> 'All About',
			);
		
			$args = array(
				'labels'              => $labels,
				'hierarchical'        => false,
				// 'description'         => 'description',
				// 'taxonomies'          => array(),
				'public'              => false,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => null,
				'menu_icon'           => 'dashicons-admin-home',
				'show_in_nav_menus'   => false,
				'publicly_queryable'  => true,
				'exclude_from_search' => true,
				'has_archive'         => false,
				'query_var'           => true,
				'can_export'          => true,
				'rewrite'             => false,
				'show_in_rest'		=> false,
				'capability_type'     => 'post',
				'supports'            => array(
					'title',
					// 'editor',
					// 'author',
					// 'thumbnail',
					'excerpt',
					
				)
				
			);
		
			register_post_type( 'about', $args );




		// Post Slider
		$labels = array(
			'name'               => __( 'Slider', 'slider' ),
			'singular_name'      => __( 'Slider', 'slider' ),
			'add_new'            => _x( 'Add New Slider', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Slider', 'slider' ),
			'edit_item'          => __( 'Edit Slider', 'slider' ),
			'new_item'           => __( 'New Slider', 'slider' ),
			'view_item'          => __( 'View Slider', 'slider' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Slider', 'slider' ),
			'all_items'			=> 'All Sliders',
		);
	
		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-image-flip-horizontal',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'show_in_rest'		=> false,
			'supports'            => array(
				'title',
				// 'editor',
				// 'author',
				// 'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'slider', $args );

		// Post Contact Us
		$labels = array(
			'name'               => __( 'Contact Us', 'contact' ),
			'singular_name'      => __( 'Contact Us', 'contact' ),
			'add_new'            => _x( 'Add New Contact Us', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Contact Us', 'contact' ),
			'edit_item'          => __( 'Edit Contact', 'contact' ),
			'new_item'           => __( 'New Contact', 'contact' ),
			'view_item'          => __( 'View Contact', 'contact' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Contact Us', 'contact' ),
			'all_items'			=> 'All Contacts Us',
		);
	
		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-email-alt',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'show_in_rest'		=> false,
			'supports'            => array(
				'title',
				// 'editor',
				// 'author',
				// 'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'contact', $args );


	}
	
	add_action( 'init', 'innovate_theme_post_types' );