<?php 

function innovate_files() {
    wp_enqueue_script('innovate-jquery-js', 'https://code.jquery.com/jquery-1.11.0.min.js');
    wp_enqueue_script('innovate-jquery-migrate-js', 'https://code.jquery.com/jquery-migrate-1.2.1.min.js');
    wp_enqueue_script('innovate-slick-js', get_theme_file_uri('/js/slick.min.js'), NULL, '1.0', true);
    wp_enqueue_script('innovate-main-js', get_theme_file_uri('/js/script.js'), NULL, microtime(), true);

    wp_enqueue_style('custom-google-fonts-Poppins', 'https://fonts.googleapis.com/css2?family=Poppins&display=swap');
    wp_enqueue_style('custom-google-fonts-FontAwesome', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css');
    wp_enqueue_style('innovate-slick_styles', get_theme_file_uri('/css/slick.css'), NULL, microtime());
    wp_enqueue_style('innovate-main_styles', get_stylesheet_uri(), NULL, microtime());
}

add_action('wp_enqueue_scripts', 'innovate_files');

function innovate_features() {
	register_nav_menu('headerMenuLocation', 'Header Menu Location');
    add_theme_support('title-tag');
}

add_action('after_setup_theme', 'innovate_features');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}