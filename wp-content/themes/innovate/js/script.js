$(document).ready(function(){
   
    

    $('.carousel__inner').slick({
            vertical: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            asNavFor: '.carousel__divider',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false,
                        arrows: false,
                        
                        
                    }
                }
            ]
         
    });

    $('.carousel__divider').slick({
        autoplay: true,
        vertical: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.carousel__inner',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        arrows: false,
        
      });

      // testimonials
      $('.testimonial__slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev"><img src="wp-content/themes/innovate/icons/prevArrow.png"></button>',
        nextArrow: '<button type="button" class="slick-next"><img src="wp-content/themes/innovate/icons/nextArrow.png"></button>',
        
     
        });
  });

  window.addEventListener('DOMContentLoaded', () => {
    const menu = document.querySelector('.menu'),
    menuItem = document.querySelectorAll('li'),
    hamburger = document.querySelector('.hamburger');

    hamburger.addEventListener('click', () => {
        hamburger.classList.toggle('hamburger_active');
        menu.classList.toggle('menu_active');
    });

    menuItem.forEach(item => {
        item.addEventListener('click', () => {
            hamburger.classList.toggle('hamburger_active');
            menu.classList.toggle('menu_active');
        })
    })
})




