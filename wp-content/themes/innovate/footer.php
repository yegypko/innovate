
<footer class="footer">
        <div class="footer__logo">
            <img src="<?php the_field('icon_footer', 'option'); ?>" alt="logo">
        </div>
        <div class="footer__contact">
            <p><?php the_field('contact_info_1', 'option'); ?></p>
            <p><?php the_field('contact_info_2', 'option'); ?></p>
        </div>
        <div class="footer__social">
            <a href="<?php the_field('link_twitter', 'option'); ?>"><img src="<?php echo get_theme_file_uri('icons/twitter.png') ?>" alt=""></a>
            <a href="<?php the_field('link_fb', 'option'); ?>"><img src="<?php echo get_theme_file_uri('icons/fb.png') ?>" alt=""></a>
            <a href="<?php the_field('link_linkedin', 'option'); ?>"> <img src="<?php echo get_theme_file_uri('icons/linked.png') ?>" alt=""></a>
        </div>
        <div class="footer__copi">
            <p><?php the_field('copyright', 'option'); ?></p>
        </div>
</footer>
<?php wp_footer();?>
</body>
</html>
