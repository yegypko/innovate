<?php get_header(); ?>
<?php 
    $frontpagePromo = new WP_Query (array(
    // Post & Page Parameters
                'posts_per_page'=> 1,
                'post_type'   => 'promo',
    ));
    while ( $frontpagePromo->have_posts()) {
    $frontpagePromo->the_post('promo'); ?>

        <section class="promo" style="background: url(<?php the_field('background_image'); ?>) center center/cover no-repeat">
            <div class="container">
                <div class="promo__wrapper">
                    <div class="promo__icons">
                        <div class="promo__icon">
                            <img src="<?php the_field('icon'); ?>" alt="image-logo">
                        </div>
                        <div class="promo__icon__text">
                            <img src="<?php the_field('icon_text'); ?>" alt="text-logo">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="advantages">
            <div class="container">
                <div class="advantages__wrapper">
                        <div class="advantages__item">
                            <img src="<?php the_field('icon_1'); ?>" alt="electronics 1" class="advantages__icon">
                            <div class="advantages__subtitle">
                                <?php the_field('title_1'); ?>
                            </div>
                            <div class="advantages__descr">
                                <?php the_field('subtitle_1'); ?>
                            </div>
                        </div>
                        <div class="advantages__item">
                            <img src="<?php the_field('icon_2'); ?>" alt="ai 1" class="advantages__icon">
                            <div class="advantages__subtitle">
                                <?php the_field('title_2'); ?>
                            </div>
                            <div class="advantages__descr">
                                <?php the_field('subtitle_2'); ?>
                            </div>
                        </div>
                        <div class="advantages__item">
                            <img src="<?php the_field('icon_3'); ?>" alt="scanner 1" class="advantages__icon">
                            <div class="advantages__subtitle">
                                <?php the_field('title_3'); ?>
                            </div>
                            <div class="advantages__descr">
                                <?php the_field('subtitle_3'); ?>
                            </div>
                        </div>
                </div>
            </div>
        </section>

    <?php } wp_reset_postdata(); ?>

    <section class="about">
        <div class="container">
            <div class="about__wrapper">
            <?php 
                  $frontpageAbout = new WP_Query (array(
                    // Post & Page Parameters
                              'posts_per_page'=> 1,
                              'post_type'   => 'about',
                  ));
                  while ( $frontpageAbout->have_posts()) {
                  $frontpageAbout->the_post('about'); ?>

                <h2 class="about__title-decor"><?php the_title();?></h2>
                <h2 class="about__title"><?php the_title();?></h2>
                <p><?php the_field('inner_text'); ?></p>
                <div class="about__info">
                    <div class="about__info__block">
                        <span><?php the_field('about_title_1'); ?></span>
                        <p><?php the_field('about_subtitle_1'); ?></p>
                    </div>
                    <div class="about__info__block">
                        <span><?php the_field('about_title_2'); ?></span>
                        <p><?php the_field('about_subtitle_2'); ?></p>
                    </div>
                    <div class="about__info__block">
                        <span><?php the_field('about_title_3'); ?></span>
                        <p><?php the_field('about_subtitle_4'); ?></p>
                    </div>
                    <div class="about__info__block">
                        <span><?php the_field('about_title_4'); ?></span>
                        <p><?php the_field('about_subtitle_4'); ?></p>
                    </div>
                </div>
                
                <p class="hide_for-smal"><?php the_excerpt(); ?></p>
                
                <?php } wp_reset_postdata(); ?>
            
            </div>
        </div>
        <div class="about__shadow">
            <div class="about__shadow__logo">
                <img src="<?php echo get_theme_file_uri('icons/Logo_smol.png') ?>" alt="logo">
            </div>

            <div class="about__shadow__rectangle">
                
            </div>
            <div class="about__shadow__circle">
                
            </div>
            
        </div>
    </section>
     
    <section class="testimonial">
        <div class="container">
            <div class="testimonial__bg">
                <div class="testimonial__wrapper">
                    <img class="quotes" src="<?php echo get_theme_file_uri('icons/quotes.png') ?>" alt="">
                    <div class="testimonial__slider">
                        <?php 
                        $frontpageTestimonial = new WP_Query (array(
                            // Post & Page Parameters
                                    'posts_per_page'=> -1,
                                    'post_type'   => 'post',
                        ));
                        while ( $frontpageTestimonial->have_posts()) {
                        $frontpageTestimonial->the_post('post'); ?>
                            <div class="testimonial__slider__content">
                                <h2><?php the_title();?></h2>
                                <span><?php the_excerpt(); ?></span>
                                <hr>
                                <p><?php the_field('testimonial_text'); ?></p>
                            </div>
                        <?php }  wp_reset_postdata(); ?>
                        
                      </div>
                </div>
            </div>
            
        </div>
    </section>

    <div class="carousel">
        <div class="container">
                <div class="carousel__divider">
                  <?php 
                  $frontpagePostSlider = new WP_Query (array(
                    // Post & Page Parameters
                              'posts_per_page'=> -1,
                              'post_type'   => 'slider',
                              'order'         => 'ASC',
                  ));
                  while ( $frontpagePostSlider->have_posts()) {
                  $frontpagePostSlider->the_post('slider'); ?>

                  <span><?php the_field('number_field'); ?></span>

                  <?php } wp_reset_postdata(); ?>
                </div>
                <div class="carousel__inner">
                  <?php 
                    $frontpagePostSlider = new WP_Query (array(
                      // Post & Page Parameters
                                'posts_per_page'=> -1,
                                'post_type'   => 'slider',
                                'order'         => 'ASC',
                    ));
                    while ( $frontpagePostSlider->have_posts()) {
                    $frontpagePostSlider->the_post('slider'); ?>
                      <div class="carousel__slide">
                          <div class="carousel__descr">
                              <div class="carousel__title">
                              <?php the_title();?>
                              </div>
                              <div class="carousel__subtitle">
                              <?php the_field('subtitle_field'); ?>
                              </div>
                          </div>
                          <div class="carousel__img">
                              <img src="<?php the_field('image_field'); ?>" alt="slide">
                          </div>
                      </div>
                    <?php } wp_reset_postdata(); ?>
                </div> 
        </div>
    </div>

    <div class="contact">
                    <?php 
                    $frontpageContact = new WP_Query (array(
                      // Post & Page Parameters
                                'posts_per_page'=> -1,
                                'post_type'   => 'contact',
                    ));
                    while ( $frontpageContact->have_posts()) {
                    $frontpageContact->the_post('contact'); ?>
                      <h2 class="contant__title-decor"><?php the_title();?></h2>
                      <h2 class="contant__title"><?php the_title();?></h2>
                      <div class="contact__form">
                          <?php the_field('contact_form'); ?>
                      </div>
                    <?php } wp_reset_postdata(); ?>
    </div>

<?php get_footer(); ?>