<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php wp_head();?>
</head>
<body>
    <header <?php body_class(); ?>>
    <header class="header">
        <a href="<?php echo site_url()?>" class="header__logo">
            <img src="<?php echo get_theme_file_uri('icons/logo.png') ?>" alt="logo">
        </a>
          <nav class="menu">
            <?php
                wp_nav_menu(array(
                  'theme_location' => 'headerMenuLocation'
                )); 
              ?>
          </nav>
           
        <div class="hamburger"></div>
    </header>